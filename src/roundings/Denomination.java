package roundings;

public class Denomination {
	
	String value;
	String level;
	
	public Denomination (String value, String level) {
		this.value = value;
		this.level = level;
	}
	
	public Denomination () {}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}
}
