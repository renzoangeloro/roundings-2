package roundings;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Initial {
		
	public Initial () throws FileNotFoundException, IOException, ParseException {
		List<Denomination> denominations = this.getDenominatios();
		String rest = this.getDataFromUser();
		System.out.print("El vuelto ingresado fue: " +rest+ ". El nuevo vuelto es: "+ this.recalculateRest(denominations, rest));
	}
	
	public int recalculateRest(List<Denomination> denominations, String rest) {
		Integer newRest = 0;
		String number = "";
		String denominationSelected = "";
		List<Integer> min = new ArrayList<Integer>();
		Iterator iterator = denominations.iterator();
		if(!(rest.equals("") && rest == null)) {
			if(rest.equals("0")) {
				return newRest;
			}else {
			while(iterator.hasNext()) {
				Denomination denm = (Denomination) iterator.next();
				if(denm.getLevel().equals("BAJO")) {
					continue;
				}else {
					min.add((this.getNumberOfRest(rest) + Integer.parseInt(denm.getValue())) - Integer.parseInt(rest));
				}
			}
			Collections.sort(min);
			newRest = Integer.parseInt(rest) + min.get(0);
			return newRest;
			}
		}else {
			return newRest;
		}
	}
	
	public int getNumberOfRest(String rest) {
		String number = "";
		List<Integer> descomp = new ArrayList<Integer>();
		for(int i = 0; i < rest.length(); i++) {
			descomp.add(rest.toCharArray()[i] - '0');
			number = number.concat(descomp.get(i).toString());
			if(descomp.size() == rest.length()) {
				int lastIndex = descomp.size() - 1;
				descomp.set(lastIndex, 0);
				number = number.substring(0,lastIndex);
				number = number.concat("0");
			}
		}
		return Integer.parseInt(number);
	}
	
	public List<Denomination> getDenominatios() throws FileNotFoundException, IOException, ParseException {
		
		Object obj = new JSONParser().parse(new FileReader("config.json")); 
        JSONObject jo = (JSONObject) obj; 
        List<Denomination> denominationList = new ArrayList();
        
        JSONArray ja = (JSONArray) jo.get("denominations"); 
        Iterator itr2 = ja.iterator(); 
          
        while (itr2.hasNext())  
        { 
            Map itr1 = ((Map) itr2.next());
            denominationList.add(new Denomination(itr1.get("denomination").toString(), itr1.get("level").toString()));
            
        }
        return denominationList;
	}
	
	
	public String getDataFromUser() {
		Scanner entry = new Scanner(System.in);
		String rest = "";
		do {
			System.out.flush();
			System.out.print("Ingrese el vuelto: ");
			rest = entry.nextLine();
		}while(!isNumeric(rest));
		return rest;
	}
	
	public boolean isNumeric(String input) {
		try {
			double data = Integer.parseInt(input);
			if(data < 0) {
				System.out.println("El numero ingresado debe ser mayor a 0.");
				return false;
			}
			return true;
		}catch(NumberFormatException e) {
			System.out.println("Debe ser un dato numerico. ");
			return false;
		}
	}
}
