package roundings;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

public class Main {

	public static void main(String[] args) throws IOException, ParseException  
    {  
		//file simulation
		JSONObject jo = new JSONObject();
		JSONArray ja = new JSONArray(); 
        
		Map m = new LinkedHashMap(2); 
        m.put("denomination", "10"); 
        m.put("level", "NORMAL"); 
        ja.add(m);
        m = new LinkedHashMap(2);
        m.put("denomination", 25); 
        m.put("level", "BAJO");  
        ja.add(m);
        m = new LinkedHashMap(2);
        m.put("denomination", 50); 
        m.put("level", "NORMAL"); 
        ja.add(m);
        m = new LinkedHashMap(2);
        m.put("denomination", 100); 
        m.put("level", "NORMAL");  
        ja.add(m);
        
        jo.put("denominations", ja);
        
        PrintWriter pw = new PrintWriter("config.json"); 
        pw.write(jo.toJSONString()); 
          
        pw.flush(); 
        pw.close(); 
        
		Initial start = new Initial();
	}
}
